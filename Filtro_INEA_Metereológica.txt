{
  "level": 0,
  "items": [
    {
      "title": "Guapiaçu Cascataí",
      "items": {
        "XX_AI_0002": "955_AI_0002",
        "XX_AI_0001": "955_AI_0001",
        "XX_AI_0003": "955_AI_0003",
        "treference": "955_AI_0008",
        "XX_graph_est": {
          "group": {
            "useRealTime": false,
            "useAutoUpdate": true,
            "intervalAutoUpdate": 300000,
            "interval": 3600000,
            "day": "line",
            "month": "column",
            "year": "column"
          },
          "series": [
            {
              "tag": "955_AI_0001",
              "alias": "Chuva ",
              "symbol": "mm",
              "titleColor": "#cccccc",
              "labelColor": "#cccccc",
              "group": {
                "day": {
                  "type": "normal",
                  "defaultClick": true
                },
                "month": {
                  "type": "sum",
                  "extract": "day"
                },
                "year": {
                  "type": "sum",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "955_AI_0002",
              "alias": "Direção do Vento",
              "symbol": "graus",
              "titleColor": "rgba(79,65,17,1)",
              "labelColor": "rgba(79,65,17,1)",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "955_AI_0003",
              "alias": "Velocidade do Vento",
              "symbol": "m/s",
              "titleColor": "rgba(19,39,214,1)",
              "labelColor": "rgba(19,39,214,1)",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "955_AI_0004",
              "alias": "Pressão Atmosférica",
              "symbol": "mbar",
              "titleColor": "rgba(255,4,4,1)",
              "labelColor": "rgba(255,4,4,1)",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "955_AI_0005",
              "alias": "Temperatura",
              "symbol": "°C",
              "titleColor": "rgba(255,123,0,1)",
              "labelColor": "rgba(255,123,0,1)",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "955_AI_0006",
              "alias": "Umidade",
              "symbol": "% UR",
              "titleColor": "rgba(99,90,151,1)",
              "labelColor": "rgba(99,90,151,1)",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "955_AI_0007",
              "alias": "Radiação",
              "symbol": "",
              "titleColor": "rgba(255,69,0,1)",
              "labelColor": "rgba(255,69,0,1)",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            }
          ]
        },
        "XX_graph_est1": {
          "group": {
            "useRealTime": false,
            "useAutoUpdate": true,
            "intervalAutoUpdate": 300000,
            "interval": 3600000,
            "day": "line",
            "month": "column",
            "year": "column"
          },
          "series": []
        },
        "device": 955,
        "XX_AI_0004": "955_AI_0004",
        "XX_AI_0005": "955_AI_0005",
        "XX_AI_0006": "955_AI_0006",
        "XX_AI_0007": "955_AI_0007"
      },
      "device": 955,
      "level": 1
    },
    {
      "title": "Juturnaíba",
      "items": {
        "XX_AI_0002": "884_AI_0002",
        "XX_AI_0001": "884_AI_0001",
        "XX_AI_0004": "884_AI_0004",
        "XX_AI_0005": "884_AI_0005",
        "XX_AI_0006": "884_AI_0006",
        "XX_AI_0007": "884_AI_0007",
        "treference": "884_AI_0008",
        "XX_graph_est": {
          "group": {
            "useRealTime": false,
            "useAutoUpdate": true,
            "intervalAutoUpdate": 300000,
            "interval": 3600000,
            "day": "line",
            "month": "column",
            "year": "column"
          },
          "series": [
            {
              "tag": "884_AI_0001",
              "alias": "Chuva ",
              "symbol": "mm",
              "titleColor": "#cccccc",
              "labelColor": "#cccccc",
              "group": {
                "day": {
                  "type": "normal",
                  "defaultClick": true
                },
                "month": {
                  "type": "sum",
                  "extract": "day"
                },
                "year": {
                  "type": "sum",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "884_AI_0002",
              "alias": "Direção do Vento",
              "symbol": "graus",
              "titleColor": "rgba(79,65,17,1)",
              "labelColor": "rgba(79,65,17,1)",
              "group": {
                "day": {
                  "type": "normal",
                  "defaultClick": true
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "884_AI_0003",
              "alias": "Velocidade do Vento",
              "symbol": "m/s",
              "titleColor": "rgba(19,39,214,1)",
              "labelColor": "rgba(19,39,214,1)",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "884_AI_0004",
              "alias": "Pressão Atmosférica",
              "symbol": "mbar",
              "titleColor": "rgba(255,4,4,1)",
              "labelColor": "rgba(255,4,4,1)",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "884_AI_0005",
              "alias": "Temperatura",
              "symbol": "°C",
              "titleColor": "rgba(255,123,0,1)",
              "labelColor": "rgba(255,123,0,1)",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "884_AI_0006",
              "alias": "Umidade",
              "symbol": "% UR",
              "titleColor": "rgba(99,90,151,1)",
              "labelColor": "rgba(99,90,151,1)",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "884_AI_0007",
              "alias": "Radiação",
              "symbol": "",
              "titleColor": "rgba(255,69,0,1)",
              "labelColor": "rgba(255,69,0,1)",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            }
          ]
        },
        "XX_graph_est1": {
          "group": {
            "useRealTime": false,
            "useAutoUpdate": true,
            "intervalAutoUpdate": 300000,
            "interval": 3600000,
            "day": "line",
            "month": "column",
            "year": "column"
          },
          "series": []
        },
        "device": 884,
        "XX_AI_0003": "884_AI_0003"
      },
      "device": 884,
      "level": 1
    }
  ],
  "title": "UHE"
}