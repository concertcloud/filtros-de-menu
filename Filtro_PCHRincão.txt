{
  "items": [
    {
      "title": "PCH Rincão Barramento",
      "items": {
        "XX_AI_0001": "280_AI_0001",
        "XX_AI_0002": "280_AI_0002",
        "XX_AI_0003": "280_AI_0003",
        "XX_AI_0004": "280_AI_0004",
        "XX_AI_0005": "280_AI_0005",
        "XX_AI_0006": "280_AI_0006",
        "device": 280,
        "XX_graph_est": {
          "group": {
            "useRealTime": false,
            "useAutoUpdate": true,
            "intervalAutoUpdate": 300000,
            "interval": 3600000,
            "day": "line",
            "month": "column",
            "year": "column"
          },
          "series": [
            {
              "tag": "280_AI_0002",
              "alias": "Nível",
              "symbol": "cm",
              "titleColor": "#54d168",
              "labelColor": "#54d168",
              "group": {
                "day": {
                  "type": "normal",
                  "defaultClick": true
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "280_AI_0004",
              "alias": "Nível Corrigido",
              "symbol": "cm",
              "titleColor": "#297531",
              "labelColor": "#297531",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "280_AI_0001",
              "alias": "Chuva",
              "symbol": "mm",
              "titleColor": "#cccccc",
              "labelColor": "#cccccc",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "sum",
                  "extract": "day"
                },
                "year": {
                  "type": "sum",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "280_AI_0005",
              "alias": "Vazão",
              "symbol": "m³/s",
              "titleColor": "#46bde3",
              "labelColor": "#46bde3",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "280_AI_0003",
              "alias": "Bateria",
              "symbol": "V",
              "titleColor": "#d9b432",
              "labelColor": "#d9b432",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            }
          ]
        },
        "XX_graph_est1": {
          "group": {
            "useRealTime": false,
            "useAutoUpdate": true,
            "intervalAutoUpdate": 300000,
            "interval": 3600000,
            "day": "line",
            "month": "column",
            "year": "column"
          },
          "series": [
            {
              "tag": "280_AI_0002",
              "alias": "PCH Rincão - Barramento - Nível",
              "symbol": "cm",
              "titleColor": "#54d168",
              "labelColor": "#54d168",
              "group": {
                "day": {
                  "type": "normal",
                  "defaultClick": true
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "280_AI_0004",
              "alias": "PCH Rincão - Barramento - Nível Corrigido",
              "symbol": "cm",
              "titleColor": "#297531",
              "labelColor": "#297531",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "280_AI_0001",
              "alias": "PCH Rincão - Barramento - Chuva",
              "symbol": "mm",
              "titleColor": "#cccccc",
              "labelColor": "#cccccc",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "sum",
                  "extract": "day"
                },
                "year": {
                  "type": "sum",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "280_AI_0005",
              "alias": "PCH Rincão - Barramento - Vazão",
              "symbol": "m³/s",
              "titleColor": "#46bde3",
              "labelColor": "#46bde3",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "280_AI_0003",
              "alias": "PCH Rincão - Barramento - Bateria",
              "symbol": "V",
              "titleColor": "#d9b432",
              "labelColor": "#d9b432",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "279_AI_0002",
              "alias": "PCH Rincão - Jusante - Nível",
              "symbol": "cm",
              "titleColor": "#54d168",
              "labelColor": "#54d168",
              "group": {
                "day": {
                  "type": "normal",
                  "defaultClick": true
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "279_AI_0004",
              "alias": "PCH Rincão - Jusante - Nível Corrigido",
              "symbol": "cm",
              "titleColor": "#297531",
              "labelColor": "#297531",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "279_AI_0001",
              "alias": "PCH Rincão - Jusante - Chuva",
              "symbol": "mm",
              "titleColor": "#cccccc",
              "labelColor": "#cccccc",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "sum",
                  "extract": "day"
                },
                "year": {
                  "type": "sum",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "279_AI_0005",
              "alias": "PCH Rincão - Jusante - Vazão",
              "symbol": "m³/s",
              "titleColor": "#46bde3",
              "labelColor": "#46bde3",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "279_AI_0003",
              "alias": "PCH Rincão - Jusante - Bateria",
              "symbol": "V",
              "titleColor": "#d9b432",
              "labelColor": "#d9b432",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            }
          ]
        }
      },
      "device": 280,
      "level": 1
    },
    {
      "title": "PCH Rincão Jusante",
      "items": {
        "XX_AI_0002": "279_AI_0002",
        "XX_AI_0001": "279_AI_0001",
        "XX_AI_0003": "279_AI_0003",
        "XX_AI_0004": "279_AI_0004",
        "XX_AI_0005": "279_AI_0005",
        "XX_AI_0006": "279_AI_0006",
        "XX_graph_est": {
          "group": {
            "useRealTime": false,
            "useAutoUpdate": true,
            "intervalAutoUpdate": 300000,
            "interval": 3600000,
            "day": "line",
            "month": "column",
            "year": "column"
          },
          "series": [
            {
              "tag": "279_AI_0002",
              "alias": "Nível",
              "symbol": "cm",
              "titleColor": "#54d168",
              "labelColor": "#54d168",
              "group": {
                "day": {
                  "type": "normal",
                  "defaultClick": true
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "279_AI_0004",
              "alias": "Nível Corrigido",
              "symbol": "cm",
              "titleColor": "#297531",
              "labelColor": "#297531",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "279_AI_0001",
              "alias": "Chuva",
              "symbol": "mm",
              "titleColor": "#cccccc",
              "labelColor": "#cccccc",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "sum",
                  "extract": "day"
                },
                "year": {
                  "type": "sum",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "279_AI_0005",
              "alias": "Vazão",
              "symbol": "m³/s",
              "titleColor": "#46bde3",
              "labelColor": "#46bde3",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "279_AI_0003",
              "alias": "Bateria",
              "symbol": "V",
              "titleColor": "#d9b432",
              "labelColor": "#d9b432",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            }
          ]
        },
        "XX_graph_est1": {
          "group": {
            "useRealTime": false,
            "useAutoUpdate": true,
            "intervalAutoUpdate": 300000,
            "interval": 3600000,
            "day": "line",
            "month": "column",
            "year": "column"
          },
          "series": [
            {
              "tag": "280_AI_0002",
              "alias": "PCH Rincão - Barramento - Nível",
              "symbol": "cm",
              "titleColor": "#54d168",
              "labelColor": "#54d168",
              "group": {
                "day": {
                  "type": "normal",
                  "defaultClick": true
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "280_AI_0004",
              "alias": "PCH Rincão - Barramento - Nível Corrigido",
              "symbol": "cm",
              "titleColor": "#297531",
              "labelColor": "#297531",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "280_AI_0001",
              "alias": "PCH Rincão - Barramento - Chuva",
              "symbol": "mm",
              "titleColor": "#cccccc",
              "labelColor": "#cccccc",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "sum",
                  "extract": "day"
                },
                "year": {
                  "type": "sum",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "280_AI_0005",
              "alias": "PCH Rincão - Barramento - Vazão",
              "symbol": "m³/s",
              "titleColor": "#46bde3",
              "labelColor": "#46bde3",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "280_AI_0003",
              "alias": "PCH Rincão - Barramento - Bateria",
              "symbol": "V",
              "titleColor": "#d9b432",
              "labelColor": "#d9b432",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "279_AI_0002",
              "alias": "PCH Rincão - Jusante - Nível",
              "symbol": "cm",
              "titleColor": "#54d168",
              "labelColor": "#54d168",
              "group": {
                "day": {
                  "type": "normal",
                  "defaultClick": true
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "279_AI_0004",
              "alias": "PCH Rincão - Jusante - Nível Corrigido",
              "symbol": "cm",
              "titleColor": "#297531",
              "labelColor": "#297531",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "279_AI_0001",
              "alias": "PCH Rincão - Jusante - Chuva",
              "symbol": "mm",
              "titleColor": "#cccccc",
              "labelColor": "#cccccc",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "sum",
                  "extract": "day"
                },
                "year": {
                  "type": "sum",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "279_AI_0005",
              "alias": "PCH Rincão - Jusante - Vazão",
              "symbol": "m³/s",
              "titleColor": "#46bde3",
              "labelColor": "#46bde3",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            },
            {
              "tag": "279_AI_0003",
              "alias": "PCH Rincão - Jusante - Bateria",
              "symbol": "V",
              "titleColor": "#d9b432",
              "labelColor": "#d9b432",
              "group": {
                "day": {
                  "type": "normal"
                },
                "month": {
                  "type": "avg",
                  "extract": "day"
                },
                "year": {
                  "type": "avg",
                  "extract": "month"
                }
              }
            }
          ]
        },
        "device": 279
      },
      "device": 279,
      "level": 1
    }
  ],
  "level": 0,
  "title": "PCH "
}